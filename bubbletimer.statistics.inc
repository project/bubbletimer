<?php


/**
 * @file
 * Bubbletimer statistic reports
 * Features:
 * 	- csv export
 */


/**
 * Page rendering
 * @return string
 */
function bubbletimer_statistics_csv_page() {
  drupal_add_js('misc/collapse.js');
  $output = t('
  	<p>Create your report. Select a form to get different statistics.</p>
  	<p>Default interval is the current month.</p>
  ');
  $output .= drupal_get_form('bubbletimer_oneuser_statistics_form');
  $output .= drupal_get_form('bubbletimer_onenode_statistics_form');
  
  return $output;
}


/*
function bubbletimer_statistics_chart_page() {
  $output  = '';
  $output .= 'Charts';
  
  return $output;
}
*/


/**
 * Form definition
 * @param array $form_state
 * @return string
 */
function bubbletimer_oneuser_statistics_form($form_state) {
  global $user;
  
  $form['bt_fieldset_default'] = array(
    '#type' => 'fieldset',
    '#title' => t('One user\'s activity'),
    '#description' => t('You can select 1 user and get all the activities that the user marked.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['bt_fieldset_default']['bt_stat_date_from'] = array(
    '#title' => t('From'),
    '#type' => 'date',
    '#default_value' => array(
      'day' => 1,
      'month' => date('n'),
      'year' => date('Y'),
    ),
  );
  $form['bt_fieldset_default']['bt_stat_date_to'] = array(
    '#title' => t('To'),
    '#type' => 'date',
    '#default_value' => array(
      'day' => date('t', time()),
      'month' => date('n'),
      'year' => date('Y'),
    ),
  );
  
  if (user_access(BT_PERM_STAT_ANY, $user)) {
    $result = db_query('SELECT uid, name FROM {users} WHERE length(name) > 0 ORDER BY uid ASC;');
    $users = array();
    while ($row = db_fetch_object($result)) {
      $users[$row->uid] = $row->name;
    }
    $form['bt_fieldset_default']['bt_user'] = array(
      '#type' => 'select',
      '#title' => t('Select user'),
      '#options' => $users,
      '#default_value' => $user->uid,
    );
  }
  
  $form['bt_fieldset_default']['bt_fieldset_extra'] = array(
    '#type' => 'fieldset',
    '#title' => 'Format options',
    '#attributes' => array('class' => 'collapsible collapsed'),
  );
  $decimal_separators = _bt_get_decimal_separators();
  $form['bt_fieldset_default']['bt_fieldset_extra']['bt_decimal_separator'] = array(
    '#type' => 'select',
    '#title' => 'Decimal separator',
    '#options' => $decimal_separators,
  );
  
  $form['bt_fieldset_default']['bt_stat_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Generate',
  );
  
  return $form;
}


/**
 * Implementation of hook_submit()
 * @param array $form
 * @param array $form_state
 */
function bubbletimer_oneuser_statistics_form_submit($form, $form_state) {
  global $user;

  list($date_from, $date_to) = _bt_get_form_state_date_range($form_state);

  if (user_access(BT_PERM_STAT_ANY, $user) && !empty($form_state['values']['bt_user'])) {
    $uid = intval($form_state['values']['bt_user']);
  }
  else {
    $uid = $user->uid; 
  }
  
  $decimal_separators= _bt_get_decimal_separators();
  $decimal_separator = substr($decimal_separators[$form_state['values']['bt_decimal_separator']], 0, 1);
  
  bubbletimer_create_oneuser_csv($date_from, $date_to, $uid, $decimal_separator);
}


/**
 * Form to get stat about one node
 * 
 * @param array $form_state
 * @return array
 */
function bubbletimer_onenode_statistics_form($form_state) {
  global $user;
  
  if (!user_access(BT_PERM_STAT_ANY)) {
    return NULL;
  }
  
  $form['bt_fieldset_default'] = array(
    '#type' => 'fieldset',
    '#title' => t('One activity\'s statistics'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Select one node and user (or each users) and get all the activities related the certain node.'),
  );
  $form['bt_fieldset_default']['bt_stat_date_from'] = array(
    '#title' => t('From'),
    '#type' => 'date',
    '#default_value' => array(
      'day' => 1,
      'month' => date('n'),
      'year' => date('Y'),
    ),
  );
  $form['bt_fieldset_default']['bt_stat_date_to'] = array(
    '#title' => t('To'),
    '#type' => 'date',
    '#default_value' => array(
      'day' => date('t', time()),
      'month' => date('n'),
      'year' => date('Y'),
    ),
  );
  
  $result = db_query('SELECT nid, title FROM {node_revisions} WHERE length(title) > 0 ORDER BY nid ASC;');
  $nodes = array();
  while ($row = db_fetch_object($result)) {
    $nodes[$row->nid] = $row->title;
  }
  $form['bt_fieldset_default']['bt_node'] = array(
    '#type' => 'select',
    '#title' => t('Select node'),
    '#options' => $nodes,
  );
  
  $result = db_query('SELECT uid, name FROM {users} WHERE length(name) > 0 ORDER BY uid ASC;');
  $users = array(0 => 'each users', -1 => '-----------');
  while ($row = db_fetch_object($result)) {
    $users[$row->uid] = $row->name;
  }
  $form['bt_fieldset_default']['bt_user'] = array(
    '#type' => 'select',
    '#title' => t('Select user'),
    '#options' => $users,
    //'#default_value' => $user->uid,
   	'#default_value' => 0,
  );
  
  $form['bt_fieldset_default']['bt_fieldset_extra'] = array(
    '#type' => 'fieldset',
    '#title' => 'Format options',
    '#attributes' => array('class' => 'collapsible collapsed'),
  );
  $decimal_separators = _bt_get_decimal_separators();
  $form['bt_fieldset_default']['bt_fieldset_extra']['bt_decimal_separator'] = array(
    '#type' => 'select',
    '#title' => 'Decimal separator',
    '#options' => $decimal_separators,
  );
  
  $form['bt_fieldset_default']['bt_stat_submit'] = array(
    '#type' => 'submit',
    '#value' => 'Generate',
  );
  
  return $form;
}


/**
 * Implementation of hook_validation()
 * @param array $form
 * @param array $form_state
 */
function bubbletimer_onenode_statistics_form_validate($form, $form_state) {
  if ($form_state['values']['bt_user'] == -1) {
    form_set_error('bt_user', t('You have to select one user or the \'each users\' option'));
  }
}


/**
 * Implementation of hook_submit()
 * @param array $form
 * @param array $form_state
 */
function bubbletimer_onenode_statistics_form_submit($form, $form_state) {
  if (!user_access(BT_PERM_STAT_ANY)) {
    return;
  }
  
  list($date_from, $date_to) = _bt_get_form_state_date_range($form_state);

  $nid = intval($form_state['values']['bt_node']);
    
  $uid = intval($form_state['values']['bt_user']);
  
  $decimal_separators= _bt_get_decimal_separators();
  $decimal_separator = substr($decimal_separators[$form_state['values']['bt_decimal_separator']], 0, 1);
  
  bubbletimer_create_onenode_csv($date_from, $date_to, $nid, $uid, $decimal_separator);
}


/**
 * Generate CSV file from submitted parameters and terminate as a downloadable file
 * This function about one user's statistic
 * 
 * @param string $date_from
 * @param string $date_to
 * @param integer $uid
 * @param char $decimal_separator
 */
function bubbletimer_create_oneuser_csv($date_from, $date_to, $uid, $decimal_separator = '.') {
  // get nodes only user have some marks on it
  $resource = db_query(
  	'SELECT DISTINCT nid 
  	FROM {bubbletimer_activity} 
  	WHERE uid = %d AND date >= \'%s\' AND date <= \'%s\';', 
    $uid,
    $date_from,
    $date_to
  );
  $nids = array();
  while ($row = db_fetch_object($resource)) {
    $nids[] = intval($row->nid);
  }

  $user = user_load(array('uid' => $uid));
  $user_name = $user->name;

  if (empty($nids)) {
    drupal_set_message(t('No information found about @name from @from to @to.',array('@name' => $user_name, '@from' => $date_from, '@to' => $date_to)));
    return;
  }
  
  $resource = db_query(
    'SELECT t.*, u.name, n.title
  	FROM ({bubbletimer_activity} t LEFT JOIN {users} u ON t.uid = u.uid) LEFT JOIN {node} n ON n.nid = t.nid
  	WHERE t.date >= \'%s\' AND t.date <= \'%s\' AND t.uid = %d AND t.nid IN (%s) 
  	ORDER BY t.date ASC;',
    $date_from,
    $date_to,
    $uid,
    join(',', $nids)
  );
  
  $result = array();
  $nodes = array();
  while ($row = db_fetch_object($resource)) {
    $result[strtotime($row->date)][$row->nid] = _bt_activity_hour_total($row->activity);
    if (!array_key_exists($row->nid, $nodes)) {
      $nodes[$row->nid] = $row->title;
    }
  }
  
  if (empty($result)) {
    drupal_set_message(t('No information found about @name from @from to @to.',array('@name' => $user_name, '@from' => $date_from, '@to' => $date_to)));
    return;
  }

  $csv = '"'. $user_name .'\'s activity",';
  $start_ts = strtotime($date_from);
  $end_ts = strtotime($date_to);
  foreach ((array)$nodes as $title) {
    $csv .= '"'. $title .'",';
  }
  $csv .= "\n";
  for ($timestamp = $start_ts; $timestamp <= $end_ts; $timestamp += BT_DAY) {
    $csv .= date('Y-m-d,', $timestamp);
    foreach ((array)$nodes as $nid => $title) {
      if (empty($result[$timestamp][$nid])) {
        $value = 0;
      }
      else {
        $value = number_format($result[$timestamp][$nid], 2, $decimal_separator, '');
      }
      $csv .= '"'. $value .'",';
    }
    $csv .= "\n";
  }
  
  // Create downloadable content
  _bt_generate_csv_to_download($csv, 'bubbletimer_'. $user_name .'_'. $date_from .'-'. $date_to   .'.csv');
}


/**
 * Build a CSV file about one node
 * 
 * @param string $date_from
 * @param string $date_to
 * @param integer $nid
 * @param string $uid
 * @param char $decimal_separator
 */
function bubbletimer_create_onenode_csv($date_from, $date_to, $nid, $uid, $decimal_separator) {
  if (!empty($uid) && is_numeric($uid) && $uid > 0) {
    $where_uid = 'AND b.uid = '.intval($uid);
  } else {
    $resource = db_query(
      'SELECT DISTINCT uid 
      FROM {bubbletimer_activity} 
      WHERE date >= \'%s\' AND date <= \'%s\' AND nid = %d;',
      $date_from,
      $date_to,
      intval($nid)
    );
    $uids = array();
    while ($row = db_fetch_object($resource)) {
      $uids[] = intval($row->uid);
    }
    $where_uid = 'AND b.uid IN ('.join(',', $uids).')';
  }
  
  $resource = db_query(
  	'SELECT b.*, u.name
  	FROM {bubbletimer_activity} b LEFT JOIN {users} u ON b.uid = u.uid
  	WHERE b.date >= \'%s\' AND b.date <= \'%s\' AND b.nid = %d '.$where_uid,
    $date_from,
    $date_to,
    intval($nid)
  );
  
  $result = array();
  $users = array();
  while ($row = db_fetch_object($resource)) {
    $result[strtotime($row->date)][$row->uid] = _bt_activity_hour_total($row->activity);
    $users[$row->uid] = $row->name;
  }

  $node_info = db_fetch_object(db_query('SELECT title FROM {node_revisions} WHERE nid = %d;', $nid));
  $node_title = $node_info->title;
  
  if (empty($result)) {
    drupal_set_message(t('No activity on @title between @from and @to', array('@title' => $node_title, '@from' => $date_from, '@to' => $date_to)));
    return;
  }
  
  $csv = '"node: '.$node_title.'",';
  $start_ts = strtotime($date_from);
  $end_ts = strtotime($date_to);
  foreach ((array)$users as $user_name) {
    $csv .= '"'. $user_name .'",';
  }
  $csv .= "\n";
  for ($timestamp = $start_ts; $timestamp <= $end_ts; $timestamp += BT_DAY) {
    $csv .= date('Y-m-d,', $timestamp);
    foreach ((array)$users as $uid => $user_name) {
      if (empty($result[$timestamp][$uid])) {
        $value = 0;
      }
      else {
        $value = number_format($result[$timestamp][$uid], 2, $decimal_separator, '');
      }
      $csv .= '"'. $value .'",';
    }
    $csv .= "\n";
  }
  
  // Create downloadable content
  _bt_generate_csv_to_download($csv, 'bubbletimer_'. $node_title .'_'. $date_from .'-'. $date_to   .'.csv');
}


/**
 * Count time from activity pattern
 * 0 means nothing
 * 1 means 15 minutes
 * @param string $pattern
 * @return float
 */
function _bt_activity_hour_total($pattern) {
  $length = strlen($pattern);
  $total = 0;
  for ($i = 0; $i < $length; $i++) {
    if (substr($pattern, $i, 1) == '1') {
      $total += 0.25;
    }
  }
  return $total;
}


/**
 * Decimal separator array
 * Drupal can't get global arrays
 * First char of the values are the separators
 * @return array
 */
function _bt_get_decimal_separators() {
  return array(
    1 => '. (period)',
    2 => ', (comma)',
  );
}


/**
 * Create a downloadable file from content
 * @param string $content
 * @param string $filename
 */
function _bt_generate_csv_to_download($content, $filename) {
  drupal_set_header('Content-Type: text/x-comma-separated-values');
  drupal_set_header('Content-Disposition: attachment; filename="'.$filename.'"');
  echo $content;
  module_invoke_all('exit');
  exit;
}


/**
 * Build the date range from form's state
 * @param array $form_state
 * @return array
 */
function _bt_get_form_state_date_range($form_state) {
  $date_from = 
    $form_state['values']['bt_stat_date_from']['year'] .'-'.
    sprintf("%02d", $form_state['values']['bt_stat_date_from']['month']) .'-'.
    sprintf("%02d", $form_state['values']['bt_stat_date_from']['day']);
    
  $date_to = 
    $form_state['values']['bt_stat_date_to']['year'] .'-'.
    sprintf("%02d", $form_state['values']['bt_stat_date_to']['month']) .'-'.
    sprintf("%02d", $form_state['values']['bt_stat_date_to']['day']);
    
  return array($date_from, $date_to);
}