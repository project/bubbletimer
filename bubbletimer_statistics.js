
/**
 * Update statistics
 * It has a 3 seconds delay to prevent being blocked by google-chart
 *  - time sums
 *  - google chart
 * @return
 */
function bt_update_stats() {
  // Update stats
  bt_throbber_on();
  clearTimeout(bt_update_delay);
  if (bt_first_reload) {
    _bt_update_stats_batch();
    bt_first_reload = false;
  } else {
    bt_update_delay = setTimeout("_bt_update_stats_batch();", BT_UPDATE_DELAY_MS);
  }
}


/**
 * Call updaters
 * @return
 */
function _bt_update_stats_batch() {
  bt_update_activity_percent();
  bt_update_chart();
}


// @TODO separate stats to another js
/**
 * Update node titles in the timesheet, how much percent of usage those has
 */
function bt_update_activity_percent() {
  var checkbox_array = $('input:checkbox:checked');
  var job_checked_array = [];
  $.each(checkbox_array, function() {
    job = $(this).attr('name').replace(bt_job_timestamp_regexp, "$1");
    if (job) {
      if (!job_checked_array[job]) {
        job_checked_array[job] = 0;
      }
      job_checked_array[job]++;
    }
  });
  $.each(bt_job_array, function() {
    hours = job_checked_array[this] * 0.25;
    $('span#bt_node_percent_' + this).html((hours || 0).toString());
  });

  $('span.bt_sum_hour').html(((checkbox_array.length * 0.25) || 0).toString());
}


/**
 * Update google chart
 * @return
 */
function bt_update_chart() {
  if (!bt_piechart_enable && !bt_barchart_enable) {
    bt_throbber_off();
    return;
  }
  
  var url_pie = 'http://chart.apis.google.com/chart?chs=400x100&cht=p3&chco=0072b9,E8B305,E8003A,6DE809&';
  var url_bar = 'http://chart.apis.google.com/chart?cht=bvg&chco=FF2A2A,FFBE33,AAFF33,36FFE1,365BFF,AE35FF,FF36A1,FF8383,FFD884,BAFF81,84FFD0,88B4FF,BB84FF,FF88DD&chds=0,8&chxt=y&chxr=0,0,8,2&';
  var percent = [];
  var total = [];
  var title = [];
  var times = [];
  var sum = 0;

  $.each($('table#bt_title_wrapper tbody tr'), function() {
    time = parseFloat($('span', this).html());
    if (time > 0) {
      title.push(encodeURIComponent($('a:last', this).html()));
      times.push(time);
      sum += time;
    }
  });

  if (times.length == 0) {
    bt_throbber_off();
    $('div#bt_chart_placeholder').html('');
    return;
  }

  $.each(times, function() {
    percent.push(100 * (this / sum));
    total.push(this);
  });
  
  percent_url_pie = percent.join(',');
  total_url_bar   = total.join('|');
  title_url       = title.join('|');
  
  url_pie = url_pie + 'chd=t:' + percent_url_pie + '&chl=' + title_url;
  url_bar = url_bar + 'chs=540x' + (title.length * 18 > 100 ? title.length * 18 : 100) + '&chd=t:' + total_url_bar + '&chdl=' + title_url;
  pie_img = bt_piechart_enable ? '<img src="'+ url_pie + '" />' : '';
  bar_img = bt_barchart_enable ? '<img src="'+ url_bar + '" />' : '';
  $('div#bt_chart_placeholder').html(pie_img + bar_img);
  $('div#bt_chart_placeholder img').load(function() {bt_throbber_off();});
}