// How much time the stat update should be delayed
var BT_UPDATE_DELAY_MS = 3000;
var BT_SECONDS_INTERVAL = 15 * 60; // 15 minutes
//SHIFT - for interval selection
var BT_INTERVAL_KEY_CODE = 16;
//CTRL - for multijob selections
var BT_MULTI_KEY_CODE = 17;
var BT_CHECKBOX_SELECTOR = 'table.bt_checkbox div input:checkbox';

// Finding a checkbox which timestamp its related to
var bt_job_timestamp_regexp = new RegExp(/^bt_job_(\d*)_timestamp_(\d*)$/g);
// Check if keys are down
var interval_key_is_active = false;
var multi_key_is_active = false;
// setTimeout var
var bt_update_delay;
// true if changes occured on the form
var bt_update_need_save_flag = false;
// nodes
var bt_job_array = [];
// true if charts isnt refreshed
var bt_first_reload = true;
// last selected slot
var bubbletimer_last_selected = false;

var bubbletimer_history_backward_array = [];
var bubbletimer_history_forward_array  = [];

var bt_checkbox_image_on;
var bt_checkbox_image_off;

var BUBBLETIMER_SLOT_UNSELECT = -1;
var BUBBLETIMER_SLOT_DEFAULT_SELECT = 0;
var BUBBLETIMER_SLOT_SELECT   = 1;
var BUBBLETIMER_NO_HISTORY_SAVE = 1;


if (Drupal.jsEnabled) {
  $(function() {
    bt_checkbox_image_on  = bt_image_path + '/cb_on.gif';
    bt_checkbox_image_off = bt_image_path + '/cb_off.gif';
    
    // Colorize
    $("input:checkbox[zebra='odd']").parent().parent().css('background', '#D0EDF7');
    // Setup width
    $('.title_hour').parent().css('width', (100 / $('.title_hour').length) + '%');
    
    bt_convert_checkboxes_to_images();
    
    if (bt_piechart_enable || bt_barchart_enable) {
      $('#bt_chart_title').show();
    }
    
    // Scroll bubbletimer to 8 o'clock
    // Sorry for this ugly hardcode
    // @TODO do something with it
    //$('div#bt_scroller').scrollLeft(544);

    // Attach key event listeners
    $(window).keydown(function(event){
      switch (event.keyCode) {
        case BT_INTERVAL_KEY_CODE:
          interval_key_is_active = true;
          multi_key_is_active = false;
          break;
        case BT_MULTI_KEY_CODE:
          multi_key_is_active = true;
          interval_key_is_active = false;
          break;
        default:
          interval_key_is_active = false;
          multi_key_is_active = false;
          break;
      }
    });
    $(window).keyup(function(event){
      interval_key_is_active = false;
      multi_key_is_active = false;
      bt_multiselect_fst_item = null;
    });

    // Cache node ids
    $.each($('a.job_node'), function() {
      bt_job_array.push($(this).attr('nid'));
    });

    // Update stats
    bt_update_stats();
  });
}
	

/**
 * Handle a checkbox click event
 * If CTRL is down, multiple selection allowed, otherwise doesn't
 * @return
 */
function bubbletimer_slot_toggle() {
  // Setup a notifier to make us save the changes
  if (!bt_update_need_save_flag) {
    bt_update_need_save_flag = true;
    // @TODO made a template from it
    $('#bt_activity_wrapper').after('<div id="bt_notify_to_save" class="messages warning">Your timesheet changed. Don\'t forget to save it!</div>');
    $('div#bt_notify_to_save').show('slow');
  }
  
  var img = $(this);
  
  // Multi task selection
  if (multi_key_is_active) {
    _bubbletimer_slot_select.apply(img);
    return;
  }  
  
  var div = img.parent();
  var checkbox = $(':checkbox', div);
  
  // Interval selection
  if (interval_key_is_active) {
    if (bubbletimer_last_selected) {
      var last_checkbox = $(':checkbox', bubbletimer_last_selected.parent());
      // Finish a multiselection
      var nid = checkbox.attr('nid');
      // Only in the same line
      if (last_checkbox.attr('nid') == nid) {
        var timestamp_from = Math.min(checkbox.attr('timestamp'), last_checkbox.attr('timestamp'));
        var timestamp_to   = Math.max(checkbox.attr('timestamp'), last_checkbox.attr('timestamp'));
        _bubbletimer_select_range(timestamp_from, timestamp_to, nid);
        bt_update_stats();
      }
    }
    return;
  }
  
  // Need manual help to unselect a slot
  if (checkbox.attr('checked')) {
    _bubbletimer_slot_select.apply(img);
    bt_update_stats();
    return;
  }
  
  // Only 1 checked slot in one column
  var timestamp = checkbox.attr('timestamp');
  $.each($('img', $("input:checkbox:checked[@timestamp='" + timestamp + "']").parent()), function() {
    _bubbletimer_slot_select.apply(this, [BUBBLETIMER_SLOT_UNSELECT]);
  });
  
  // Click event
  _bubbletimer_slot_select.apply(img);
  // Update stat
  bt_update_stats();
}


/**
 * Higlight the related titles for checboxes you are onmouseover
 * @param object oItem
 * @param boolean is_highlight
 * @return
 */
function bt_highlight_titles(event) {
  var is_highlight = event.data.is_highlight;
  var checkbox = $(':checkbox', this);
  var hour = checkbox.attr('hour');
  var nid  = checkbox.attr('nid');
  // @TODO make selector define{forced_select: true}
  if (is_highlight) {
    $("a[nid='" + nid + "']").parent().addClass('bt_title_highlight');
    $("div[hour='" + hour + "']").addClass('bt_title_highlight');
  } else {
    $("a[nid='" + nid + "']").parent().removeClass('bt_title_highlight');
    $("div[hour='" + hour + "']").removeClass('bt_title_highlight');
  }
}


/**
 * Loader icon on
 * @return
 */
function bt_throbber_on() {
  $('span.bt_chart_updater').addClass('occupied');
}


/**
 * Loader icon off
 * @return
 */
function bt_throbber_off() {
  $('span.bt_chart_updater').removeClass('occupied');
}


// @TODO check callee occurences - div/img/cbx
function _bubbletimer_slot_select(forced_select, _history_no_save) {
  var do_select       = forced_select     === BUBBLETIMER_SLOT_SELECT;
  var do_unselect     = forced_select     === BUBBLETIMER_SLOT_UNSELECT;
  var history_no_save = _history_no_save  === BUBBLETIMER_NO_HISTORY_SAVE;
  console.log('fs: ' + forced_select);
  var img = $(this);
  var checkbox = $(':checkbox', img.parent());
  if ((img.attr('src') == bt_checkbox_image_on && !do_select) || do_unselect) {
    console.log('unselect');
    img.attr('src', bt_checkbox_image_off);
    checkbox.attr('checked', null);
  } else {
    console.log('select');
    img.attr('src', bt_checkbox_image_on);
    checkbox.attr('checked', 'checked');
    bubbletimer_last_selected = img;
  }
  
  if (!history_no_save) {
    _bubbletimer_save_history(img);
  }
}


function bt_convert_checkboxes_to_images() {
  $.each($(BT_CHECKBOX_SELECTOR), function() {
    var checkbox = $(this);
    var div = checkbox.parent();
    
    // Hide checkboxes
    checkbox.hide();

    // Append image
    div.append('<img src="' + bt_checkbox_image_off + '" />');
    var img = $('img', div);
    img.bind('click', bubbletimer_slot_toggle);
    div.bind('mouseover', {is_highlight: true}, bt_highlight_titles);
    div.bind('mouseout', {is_highlight: false}, bt_highlight_titles);
    
    if (checkbox.attr('checked')) {
      img.attr('src', bt_checkbox_image_on);
    }
  });
}


function _bubbletimer_select_range(timestamp_from, timestamp_to, nid) {
  var history = [];
  for (var t = timestamp_from; t <= timestamp_to; t += BT_SECONDS_INTERVAL) {
    var img = $('img', $("input:checkbox[timestamp='" + t + "'][nid='" + nid + "']").parent());
    _bubbletimer_slot_select.apply(img, [BUBBLETIMER_SLOT_SELECT, BUBBLETIMER_NO_HISTORY_SAVE]);
    history.push(img);
  }
  bubbletimer_history_backward_array.pop();
  _bubbletimer_save_history(history);
}

