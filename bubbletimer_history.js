function bubbletimer_history_backward() {
  if (bubbletimer_history_backward_array.length == 0) return;
  
  var oImages = _bubbletimer_get_history();
  _bubbletimer_save_forward(oImages);
  if (oImages.length > 1) {
    for (var idx in oImages) {
      _bubbletimer_slot_select.apply(oImages[idx], [BUBBLETIMER_SLOT_DEFAULT_SELECT, BUBBLETIMER_NO_HISTORY_SAVE]);
    }
  } else {
    _bubbletimer_slot_select.apply(oImages, [BUBBLETIMER_SLOT_DEFAULT_SELECT, BUBBLETIMER_NO_HISTORY_SAVE]);
  }
}


function bubbletimer_history_forward() {
  if (bubbletimer_history_forward_array.length == 0) return;
  
  var oImages = _bubbletimer_get_forward();
  bubbletimer_history_backward_array.push(oImages);
  if (oImages.length > 1) {
    for (var idx in oImages) {
      _bubbletimer_slot_select.apply(oImages[idx], [BUBBLETIMER_SLOT_DEFAULT_SELECT, BUBBLETIMER_NO_HISTORY_SAVE]);
    }
  } else {
    _bubbletimer_slot_select.apply(oImages, [BUBBLETIMER_SLOT_DEFAULT_SELECT, BUBBLETIMER_NO_HISTORY_SAVE]);
  }
}


function _bubbletimer_save_history(history) {
  bubbletimer_history_backward_array.push(history);
  
  bubbletimer_history_forward_array = [];
}


function _bubbletimer_get_history() {
  return bubbletimer_history_backward_array.pop();
}


function _bubbletimer_save_forward(history) {
  bubbletimer_history_forward_array.push(history);
}


function _bubbletimer_get_forward() {
  return bubbletimer_history_forward_array.pop();
}