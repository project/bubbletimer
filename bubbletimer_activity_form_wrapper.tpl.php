<table id="bt_activity_wrapper">
	<tr>
		<td valign="top" align="right"><?php echo drupal_render($form['bt_title_wrapper']); ?></td>
		<td valign="top">
		  <div id="bt_scroller">
		    <?php echo drupal_render($form['bt_checkbox_wrapper']); ?>
		  </div>
		</td>
	</tr>
</table>

<?php echo drupal_render($form['bt_submit']); ?>

<h3 id="bt_chart_title">Quick daily chart</h3>
<div id="bt_chart_placeholder"></div>
